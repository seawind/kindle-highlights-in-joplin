"""Configuration file

Modify this file to access your highlights and notes in your kindle device.
    
Parameters
----------
token: str
    Your Joplin Web Clipper authorisation token.
    You can find it in Joplin accessing to the Tools -> Options menu.
    Select the Web Clipper tab and copy the token from there.

folder: str
    The name of the notebook in which you want to store your highlights and notes in Joplin.

path_highlights: str
    The path to the file where your highlights and notes are stored.
    In your Kindle device it is usually stored in the folder `Documents`.
    The name of the file is `My Clippings.txt`, or in Spanish `Mis recortes.txt`.


@author: @seawind
"""

token = 'your_token'
folder = 'Kindle highlights'
path_highlights = '/path/to/your/Kindle/My Clippings.txt'

