"""Kindle highlights configuration

This module contains the structures that allow to identify the elements of a \
note or highlight in the Kindle clippings file. 

Parameters
----------
separator: str
    A string that indicates the sequence of characters that separate highlights (default is '==========')
highlight_kw: dictionary
    A dictionary containing the keyword for 'Highlight' in different languages.
    The strings should be formatted to be used as Regular Expressions.
note_kw: dictionary
    A dictionary containing the keyword for 'Note' in different languages.
    The strings should be formatted to be used as Regular Expressions.
bookmark_kw: dictionary
    A dictionary containing the keyword for 'Bookmark' in different languages.
    The strings should be formatted to be used as Regular Expressions.
page_kw: dictionary
    A dictionary containing the keyword for 'Page' in different languages.
    The strings should be formatted to be used as Regular Expressions.
added_kw: dictionary
    A dictionary containing the words for 'Added on' in different languages.
    The strings should be formatted to be used as Regular Expressions.
added_kw: loc_kw
    A dictionary containing the words and variations for 'Location' in different languages.
    The strings should be formatted to be used as Regular Expressions.
    
    
When the kindle device has been configured in different languages along time, \
the highlights or notes are stored using keywords from different languages. \
For this reason it is useful to combine those keywords in order to find the desired patterns. 
    
highl_kws: str
    Contains the keywords for 'Highlight' from all the specified languages separated by a '|'
note_kws: str
    Contains the keywords for 'Note' from all the specified languages separated by a '|'
page_kws: str
    Contains the keywords for 'Page' from all the specified languages separated by a '|'
added: str
    Contains the keywords for 'Added on' from all the specified languages separated by a '|'
locs: str
    Contains the keywords for 'Location' from all the specified languages separated by a '|'
    
Examples
----------
This is a section of "My Clippings.txt" file containing 2 highlights:

==========
How to Take Smart Notes: One Simple Technique to Boost Writing, Learning and Thinking – for Students, Academics and Nonfiction Book Writers (Sönke Ahrens)
- Highlight on Page 38 | Loc. 686-89  | Added on Tuesday, November 26, 2019, 07:09 PM

And by doing everything with the clear purpose of writing about it, you will do what you do deliberately...
==========
Complejidad (Roger Lewin)
- Subrayado en la página 157 | Pos. 2402-4  | Añadido el viernes 3 de enero de 2014 08H52' GMT

«La capacidad de obtener y procesar información sobre el entorno,...
==========

This is one example from a kindle D01100, it might be different for other models. 
The syntaxis is as follows:
    
==========
<Title> (<Author>)
- <Highlight keyword> .. <Page keyword if applies> <Page number> | <Location keyword> <Location> | <Added on keyword> <time-date>

<Highlight or note content>
==========

@author: @seawind
"""

separator = "==========" 

# ======================= KEYWORDS IN DIFFERENT LANGUAGES ============================= #


highlight_kw = {'english': 'Highlight',
                'spanish': 'Subrayado',
                'german': 'Markierung'}

note_kw = {'english': 'Note',
           'spanish': 'Nota',
           'german': 'Notiz'}

bookmark_kw = {'english': 'Bookmark',
               'spanish': 'Marcador',
               'german': 'Lesezeichen'}

page_kw = {'english': 'Page',
           'spanish': 'página',
           'german': 'Seite'}

added_kw = {'english': 'Added on',
            'spanish': 'Añadid. el', ## Contains the wildcard (.) because it could be 'a' or 'o' 
            'german': 'Hinzugefügt am'} 

loc_kw = {'english': 'Location|Loc\.|Position', #Adds a \ before the dot to specify that it's a dot and not the wildcard . for regEx
          'spanish': 'Pos\.',
          'german': 'Position'}

### Joins the keywords with a | 

highl_kws = '|'.join([i for k,i in highlight_kw.items()])
note_kws = '|'.join([i for k,i in note_kw.items()])
page_kws = '|'.join([i for k,i in page_kw.items()])
added = '|'.join([i for k,i in added_kw.items()])
locs = '|'.join([i for k,i in loc_kw.items()])
