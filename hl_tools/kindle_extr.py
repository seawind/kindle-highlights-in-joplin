"""Kindle highlights extraction

This module contains functions that allow retrieving highlights and notes \
from a Kindle eReader device.

This module requires that `pandas` and `dateparser` be installed within the Python \
environment you are running this script in.

This file contains the following functions:
    
    * read_clippings_file - Reads the txt file and returns a list containing the individuals highlights.
    * get_highlight_data - Extracts the data of an individual highlight.
    * get_highlights -  Retrieves the data of the highlights and notes contained in the clippings file.
    * get_book_and_author - Retrieves a list of the books identified by title and author.


@author: @seawind
"""

import dateparser
from .kindle_conf import highl_kws,note_kws,locs,page_kws,added,separator
import pandas as pd
import re

# ======================= FUNCTIONS TO RETRIEVE DATA FROM A KINDLE eREADER ============================= #

def read_clippings_file(path,separator=separator):
    """Reads the txt file containing the Kindle highlights and returns a list containing the individuals highlights.
    Parameters
    ----------
    path: str
        The file location of the txt file containing the highlights and notes.
    separator: str
        A sequence of characters that separates the individual highlights in the txt file.
    
    Returns
    ----------
    list
        a list containing the individual highlights.  
    
    """
    try:
        with open(path, encoding="utf-8-sig") as f:
            data = f.read()
            highlights_list = data.split(separator)
            return highlights_list
    except:
        print(f"{path} is not accessible. Verify that the path is correct.")
        quit()
        

def get_highlight_data(hl,highl_kws=highl_kws,note_kws=note_kws,locs=locs,page_kws=page_kws,added=added):
    """Extracts the data of individual highlights
    
    This function gets the different lines from the strinn received and \
    uses regular expressions to identify the metadata given a specific syntaxis.
    
    Parameters
    ----------
    hl: str
        The full note or highlight including the metadata: title, author, date, location and page.
    highl_kws: str
        The keyword for a highlight (e.g. 'Highlight') used in the Kindle txt file (default is highl_kws imported from the `kindle_conf` module).
    note_kws: str
        The word for a note (e.g. 'Note') used in the Kindle txt file (default is note_kws imported from the `kindle_conf` module).
    locs: str
        The word for the highlight position (e.g. 'Loc.') used in the Kindle txt file (default is locs imported from the `kindle_conf` module).
    page_kws: str
        The word for page (e.g. 'Page') used in the Kindle txt file (default is page_kws imported from the `kindle_conf` module).
    added: str
        The words used in the Kindle txt file indicating when the highlight was added (e.g. 'Added on') (default is added imported from the `kindle_conf` module).
        
    Returns
    ----------
    dictionary
        A dictionary containing the data with the keys:
            title, author, type_hl (note or highlight), location, page, date_time and highlight (content)          
    
    """
    md = {}
    cont = hl.splitlines()
    cont = [md for md in cont if md != '']
    ## Book
    book = re.match(r'(?P<title>.*)\((?P<author>.*)\)', cont[0])
    md['title'] = book.group('title').strip()
    md['author'] = book.group('author').strip()
    ### defines if it's a note or a highlight -- So far, it only retrieves those elemets
    if re.search(highl_kws,cont[1]):
        md['type_hl'] = 'highlight'
    elif re.search(note_kws,cont[1]):
        md['type_hl'] = 'note'
    else:
        return None
    ## Location or position in the ebook
    loc = re.match(r'.*('+ locs + r') (?P<location>[0-9,-]+).*', cont[1])
    md['location'] = loc.group('location')
    ## Page
    try:
        pag = re.match(r'.*('+page_kws+') (?P<page>[0-9,-]+) |$', cont[1])
        md['page'] = pag.group('page')
    except:
        md['page'] = ''
    ## Date
    dateS = re.match(r".*("+added +") (?P<timestamp>.*) (?P<hour>[0-9]+)(H|:)(?P<minutes>[0-9]+)('|.)(?P<id>.*)$", cont[1])
    date = f"{dateS.group('timestamp')} {dateS.group('hour')}:{dateS.group('minutes')}"
    date = date + dateS.group('id') if dateS.group('id') in ['AM','PM'] else date
    md['date_time'] = dateparser.parse(date)
    ### Assigns the body of the highlight or note
    md['highlight'] = cont[-1]
    return md

def get_highlights(path,highl_kws=highl_kws,note_kws=note_kws,locs=locs,page_kws=page_kws,added=added):
    """Retrieves the data of the highlights and notes contained in the clippings file.
    Parameters
    ----------
    path: str
        The file location of the Kindle txt file containing the highlights and notes.
    highl_kws: str
        The keyword for a highlight (e.g. 'Highlight') used in the Kindle txt file (default is highl_kws imported from the `kindle_conf` module).
    note_kws: str
        The word for a note (e.g. 'Note') used in the Kindle txt file (default is note_kws imported from the `kindle_conf` module).
    locs: str
        The word for the highlight position (e.g. 'Loc.') used in the Kindle txt file (default is locs imported from the `kindle_conf` module).
    page_kws: str
        The word for page (e.g. 'Page') used in the Kindle txt file (default is page_kws imported from the `kindle_conf` module).
    added: str
        The words used in the Kindle txt file indicating when the highlight was added (e.g. 'Added on') (default is added imported from the `kindle_conf` module).
    
    Returns
    ----------
    Pandas DataFrame
        A data-frame containing in each row the information from each highlight or note. \
        The columns are: title, author, type_hl, location, page, date_time and highlight
    
    """
    highlights_list = read_clippings_file(path)
    highlights = pd.DataFrame(columns=['title','author','type_hl','location','page','date_time','highlight'])
    print ('Retrieving highlights')
    for hl in highlights_list:
        if hl!='\n':
            md = get_highlight_data(hl,highl_kws=highl_kws,note_kws=note_kws,locs=locs,page_kws=page_kws,added=added)
            if md:
                highlights = highlights.append(md,ignore_index=True)
    print (f'{len(highlights)} highlights retrieved from the original file.')
    return highlights

def get_book_and_author(highlights):
    """Retrieves a list of the books identified by title and author.
    
    Parameters
    ----------
    highlights: Pandas dataframe
        the data of the highlights and notes
        
    Returns
    ----------  
    list 
        A list with the different books, each element of the list has the form: 'title - author'
    """
    title_author = highlights[['title', 'author']].agg(' - '.join, axis=1).unique()
    return list(title_author)