"""Tools to create the elements of a note in Joplin from highlights obtained from a Kindle eReader device


This file contains the following functions:
    
    * create_identifier - Creates the unique identifier for each highlight in the book
    * find_missing_highlights - Finds the missing highlights in an existing note in Joplin
    * create_note_body -  Creates the body of a note in Joplin

@author: @seawind
"""

from datetime import datetime

    
def create_identifier(row):
    """Creates the unique identifier for each highlight in the book
    Parameters
    ----------
    row: Pandas data-frame row
        A row from a dataframe with the columns: type_hl, date_time, location and page
    Returns
    ----------
    string
        A string with the identifier using the format:
            <Highlight or note> added on <time-date> |Loc. <location> | Page: <page> 
    """
    ident_list = [row.type_hl.title()+' added on '+row.date_time.strftime("%d-%m-%Y %H:%M"),
                  'Loc. '+row.location] 
    if row.page != '':
        ident_list.append('Page: '+row.page)
    ident = ' | '.join(ident_list)
    return ident

def find_missing_highlights(hl_book,body):
    """Finds the missing highlights in an existing note in Joplin
    Parameters
    ----------
    hl_book: Pandas Dataframe
        A data-frame containing the highlights and notes for a specific book \
        retrieved from the kindle device
    body: str
        The body of an existing note in Joplin for the same book.
    
    Returns
    ----------
    list
        A list containing the data-frame indices for the missing notes    
    """
    missing_hls = []
    for row in hl_book.itertuples():
        if body.find(row.identifier) == -1:
            missing_hls.append(row.Index)
    return missing_hls


def create_note_body(hl_book,title,missing_hls):
    """Creates the body of a note in Joplin
    This function creates the body for the missing highlights in an existing note. \
    If the note does not exist it creates the whole body including the title.
    
    Parameters
    ----------
    hl_book: Pandas Dataframe
        A data-frame containing the highlights and notes for a specific book \
        retrieved from the kindle device
    title: str
        The title of the note (Book - Author)
    missing_hls: list
        A list with the missing highlights if the note already exists in Joplin. \
        if the note does not exist, it contains all the indices in hl_book.
        
    Returns
    ----------
    body: str
        A string with the body of the note containing the missing highlights \
        for the specific book.
    
    """
    separator = '---'
    body = '' 
    new_note = False
    if len(missing_hls) == len(hl_book): 
        title_md = "# " + title
        body += title_md
        new_note = True
    hl_book = hl_book[hl_book.index.isin(missing_hls)]    
    for i,row in enumerate(hl_book.itertuples()):
        if i != 0 or new_note == False:
            ## Adds a separator between notes
            body += '\n\n' + separator
        body += '\n### ' + row.identifier + '\n '
        body += row.highlight
        body += '\n  <sub><sup>' + '- Retrieved on ' + datetime.now().strftime("%d-%m-%Y %H:%M:%S") + '</sup></sub>'
    return body