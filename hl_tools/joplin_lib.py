"""Tools to manipulate elements in Joplin

This module contains functions that allow manipulating elements such as \
notebooks and notes in Joplin.

This module requires that `joplin_api` be installed within the Python \
environment you are running this script in. 
See https://gitlab.com/annyong/joplin-api 

This file contains the following functions:
    
    * validate_response - Validates the Joplin API response
    * new_folder - Creates a new notebook in Joplin
    * search_folder -  Searches for a notebook by its name
    * create_folder_if_not_exists - Creates a notebook if it does not exist
    * search_note - Searches for a note by its name in a given folder
    * get_note - Retrieves the content of a note 
    * create_note - Creates a note
    * update_note - Updates an existing note


@author: @seawind
"""
from joplin_api import JoplinApiSync



def validate_response(res,operation='Action '):
    """Validates the Joplin API response
    
    If the response is not valid it raises a Warning to stop the running script.
    
    Parameters
    ----------
    res: httpx.Response
        The response obtained when calling a method through the API.
    operation: str
        The type of request through the API.
    """
    if type(res.text) is str and res.status_code == 200:
        pass
    else:
        raise Warning(f"{operation} failed.")
     

def new_folder(token,folder_name):
    """Creates a new notebook in Joplin
    
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    folder_name: str
        The name of the notebook to create    
    """
    joplin = JoplinApiSync(token=token)
    res = joplin.create_folder(folder=folder_name)
    validate_response(res,'Notebook creation')
    print(f"Notebook: '{folder_name}' created successfully.")
  
def search_folder(token,folder_name):
    """Searches for a notebook by its name
    
    `search_folder` finds all notebooks with the specified name.
    If more than one notebook is found with the same name a warning is raised.
    
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    folder_name: str
        The name of the notebook to search for
    
    Returns
    ----------
    list
        a list of dictionaries containing the metadata for each notebook found. 
    """
    joplin = JoplinApiSync(token=token)
    item_type = 'folder'
    search = joplin.search(folder_name, item_type)
    validate_response(search,'Notebook search')
    folder = search.json()    
    if len(folder['items']) > 1:
        raise Warning(f"There is more than one notebook with the name: '{folder_name}'")
    return folder['items']

def create_folder_if_not_exists(token,folder_name):
    """Creates a notebook if it does not exist
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    folder_name: str
        The name of the notebook to create.
    """
    search_res = search_folder(token,folder_name)
    if len(search_res) == 0:              
        new_folder(token,folder_name)
    else:
        print(f"Notebook: '{folder_name}' already exists.")

def search_note(token,folder_name,note_name):
    """Searches for a note by its name in a given folder
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    folder_name: str
        The name of the notebook to search for the note.
    note_name: str
        The name of the note to search for.
    
    Returns
    ----------
    dictionary
        A dictionary containing the Notebook id (nb_id) and the \
        note id (note_id) in case it exists, otherwise it returns ''. 
    
    """
    joplin = JoplinApiSync(token=token)
    search_fol = search_folder(token,folder_name)
    if len(search_fol) == 0:
        ids = {'nb_id':''}
        print(f"The notebook '{folder_name}' does not exist.")
        exit
    else:
        ids = {'nb_id':search_fol[0]['id']}    
    search = joplin.search(f'"{note_name}"', item_type='note')
    validate_response(search,'Note search')
    sear_note = search.json()
    notes = sear_note['items']
    notes_in_folder = [d for d in notes if d['parent_id']==search_fol[0]['id']]
    if len(notes_in_folder) > 1:
        ### If the note is duplicated in the same folder
        raise Warning(f"There is more than one note with the name: '{note_name}' in the '{folder_name}' notebook.")
    elif len(notes_in_folder) == 1:
        ### If the note exists and is unique returns the id
        ids['note_id'] = notes_in_folder[0]['id']
    else:
        # If the note does not exist returns an empty string
        ids['note_id'] = ''
    return ids

        
def get_note(token,note_id):
    """Retrieves the content of a note 
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    note_id: str
        The id of the note to be retrieved.
    Returns
    ----------
    dictionary
        A dictionary containing the Note metadata and body content. 
    """
    joplin = JoplinApiSync(token=token)
    note = joplin.get_note(note_id)
    validate_response(note,'Note retrieving')
    return note.json()

def create_note(token,note_name,body,parent_id):
    """Creates a note
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    note_name: str
        The title of the note.
    body: str
        The content of the note.
    parent_id: str
        The id of the notebook where the note should be stored.
    Returns
    ----------
    httpx.Response
        The response from the API after requesting the creation of the note. 
    """
    joplin = JoplinApiSync(token=token)
    res = joplin.create_note(title=note_name, body=body,parent_id=parent_id)
    validate_response(res,'Note creation')
    return res

def update_note(token,note_id,note_name,body,parent_id):
    """Updates an existing note
    Parameters
    ----------
    token: str
        Your Joplin Web Clipper authorisation token.
    note_id: str
        The id of the note to be updated.
    note_name: str
        The title of the note.
    body: str
        The content of the note.
    parent_id: str
        The id of the notebook where the note is stored.
    Returns
    ----------
    httpx.Response
        The response from the API after requesting the update of the note. 
    """
    joplin = JoplinApiSync(token=token)
    res = joplin.update_note(note_id=note_id,title=note_name, body=body,parent_id=parent_id)
    validate_response(res,'Note update')
    return res