# Kindle highlights in Joplin

A Python script that imports highlights and notes from a Kindle eReader device into [Joplin](https://joplinapp.org/). It creates a notebook in Joplin and individual notes for each book inside that notebook. All the highlights and annotations for each book are stored in the same note and separated by a horizontal line. When a note for a book already exists, this script updates the note with the new highlights.

## Requirements
- python 3.7+
- httpx
- pandas
- [dateparser](https://dateparser.readthedocs.io/en/latest/)
- [joplin_api](https://gitlab.com/annyong/joplin-api)

## Download this package
```
git clone https://gitlab.com/seawind/kindle-highlights-in-joplin.git
```

## Getting your kindle highlights and annotations in Joplin
1. Access the folder `kindle-highlights-in-joplin`.
2. Open the file `conf.py` and enter your Joplin Token.
  - To find your Joplin Token, open Joplin and access to Tools -> Options -> Web Clipper. Enable the web clipper service and copy your authorisation token in the `token` variable in the `conf.py` file.
3. Define a name for the notebook in Joplin in which you want to store your highlights.
4. Connect your Kindle device and find the path to the file where your highlights and notes are stored. It is usually inside the folder `Documents`. The name of the file depends on the language in your device. In English it is `My Clippings.txt`, or in Spanish `Mis recortes.txt`.
5. Copy the path in the variable `path_highlights` in the file `conf.py`

  The file `conf.py` looks like this:
  ```
  token = 'your_token'
  folder = 'Kindle highlights'
  path_highlights = '/path/to/your/Kindle/My Clippings.txt'
  ```
6. Open a terminal in the `kindle-highlights-in-joplin` folder and run the next command:
  ```
  python sync_kindle_joplin.py
  ```
## Considerations

- This script has been tested with the highlights stored in a Kindle D01100. It might be different for other models.

- Kindle eReaders usually store the highlights and notes in a txt file. The name of the file depends on the language in which the device is configured.
- The elements stored in that file follow a specific structure, a specific sequence of characters separates individual highlights. The metadata for each highlight is written in the language of the device. This package uses regular expressions to retrieve the metadata to build a unique identifier for each highlight. So far, it includes keywords in Spanish and English to identify the relevant data. You can add keywords for a different language in the `kindle_conf` module.
- If you configure your kindle device with different languages along time, the highlights will use keywords in different languages. This package considers that special case.

The structure of the highlights in `My Clippings.txt` is as follows:

```
==========
How to Take Smart Notes: One Simple Technique to Boost Writing, Learning and Thinking – for Students, Academics and Nonfiction Book Writers (Sönke Ahrens)
- Highlight on Page 38 | Loc. 686-89  | Added on Tuesday, November 26, 2019, 07:09 PM

And by doing everything with the clear purpose of writing about it, you will do what you do deliberately...
==========
Complejidad (Roger Lewin)
- Subrayado en la página 157 | Pos. 2402-4  | Añadido el viernes 3 de enero de 2014 08H52' GMT

«La capacidad de obtener y procesar información sobre el entorno,...
==========
```
This is an example from a kindle D01100, it might be different for other models.

The syntaxis is as follows:
```
==========
<Title> (<Author>)
- <Highlight keyword> .. <Page keyword if applies> <Page number> | <Location keyword> <Location> | <Added on keyword> <time-date>

<Highlight or note content>
==========
```
## Contributions

### Adding keywords in different languages

You can contribute identifying and adding the keywords used in different languages in the file `kindle_conf.py`. They are stored in a dictionary for each relevant keyword. For example, the keywords `Highlight` and `Added on`:  

```
highlight_kw = {'english': 'Highlight',
                'spanish': 'Subrayado'}

added_kw = {'english': 'Added on',
            'spanish': 'Añadid. el'}
```

Notice that the strings should be formatted to be used as Regular Expressions. In this case, the symbol `.` represents a wildcard character meaning that the script will search the pattern `Añadid. el` no matter which character is found after `Añadid`.

### Testing different devices

If you have a different Kindle model, you could test this script with your device and report any issues.

If this script does not work with your device you could share some highlights from your txt file to enhance this script to include new structures. Or if you prefer to modify the code yourself `Merge requests` are welcome.

------------

This script is based on the great work of @foxmask who created the `joplin-api` for Python.
